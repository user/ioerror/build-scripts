#!/bin/sh

PKG=$1
VER=$2

## XXX TODO:
## The package version is currently hardcoded for ${VER}-1
## This should be detected

cp -a ${PKG}-${VER} ${PKG}-${VER}.ORIG

# Make the first dsc
dpkg-source -b ${PKG}-${VER}

for SUITE in lenny squeeze intrepid jaunty karmic lucid maverick
	do
    echo "dpkg-source for $SUITE..."
	case "$SUITE" in
		(intrepid|jaunty|karmic|lucid|maverick)
		sed -i -e "1s/${VER}-1/${VER}-1~${SUITE}1/" ${PKG}-${VER}/debian/changelog
		dpkg-source -b ${PKG}-${VER}
		ls -la ${PKG}-${VER}
		ls -la ${PKG}-${VER}.ORIG
		rm -rf ${PKG}-${VER}
		cp -a  ${PKG}-${VER}.ORIG ${PKG}-${VER}
		;;
		(*)
		sed -i -e "1s/${VER}-1/${VER}-1~${SUITE}/" ${PKG}-${VER}/debian/changelog
		dpkg-source -b ${PKG}-${VER}
		ls -la ${PKG}-${VER}
		ls -la ${PKG}-${VER}.ORIG
		rm -rf ${PKG}-${VER}
		cp -a  ${PKG}-${VER}.ORIG ${PKG}-${VER}
		;;
	esac
	done

echo "Cleaning up..."
rm -rf ${PKG}-${VER}
rm -rf ${PKG}-${VER}.ORIG
